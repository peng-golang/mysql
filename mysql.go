package mysql

import (
	"database/sql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

type GormConn struct {
	SqlDB  *sql.DB
	GormDB *gorm.DB
}

func Connect(connStr string) (*GormConn, error) {
	sqlDB, err := sql.Open("mysql", connStr)
	if err != nil {
		panic(err)
	}
	sqlDB.SetConnMaxLifetime(time.Minute * time.Duration(5))
	sqlDB.SetConnMaxIdleTime(time.Minute * time.Duration(2))
	sqlDB.SetMaxOpenConns(20)
	conf := &gorm.Config{}
	gormDB, err := gorm.Open(mysql.New(mysql.Config{Conn: sqlDB}), conf)
	if err != nil {
		return nil, err
	}
	conn := &GormConn{
		SqlDB:  sqlDB,
		GormDB: gormDB,
	}
	return conn, nil
}
func ConnectDebug(connStr string) (*GormConn, error) {
	sqlDB, err := sql.Open("mysql", connStr)
	if err != nil {
		panic(err)
	}
	sqlDB.SetConnMaxLifetime(time.Minute * time.Duration(5))
	sqlDB.SetConnMaxIdleTime(time.Minute * time.Duration(2))
	sqlDB.SetMaxOpenConns(20)
	conf := &gorm.Config{}
	conf.Logger = logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // Slow SQL threshold
			LogLevel:      logger.Info, // Log level
			Colorful:      true,        // Disable color
		},
	)
	gormDB, err := gorm.Open(mysql.New(mysql.Config{Conn: sqlDB}), conf)
	if err != nil {
		return nil, err
	}
	conn := &GormConn{
		SqlDB:  sqlDB,
		GormDB: gormDB,
	}
	return conn, nil
}
