package mysql

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

type Logger struct {
	logger *zap.Logger
}

func NewLogger(logDir string) *Logger {
	logWriter := zapcore.AddSync(&lumberjack.Logger{
		Filename:   fmt.Sprintf("%s/mysql.log", logDir),
		MaxSize:    10,
		MaxBackups: 5,
		MaxAge:     3,
		Compress:   false,
	})
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	encoder := zapcore.NewConsoleEncoder(encoderConfig)
	writeSyncer := zapcore.NewMultiWriteSyncer(zapcore.AddSync(logWriter))
	//writeSyncer := zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(logWriter))
	core := zapcore.NewCore(encoder, writeSyncer, zapcore.DebugLevel)
	return &Logger{
		logger: zap.New(core),
	}
}

func (l *Logger) Printf(format string, v ...interface{}) {
	l.logger.Sugar().Infof(format, v)
}
